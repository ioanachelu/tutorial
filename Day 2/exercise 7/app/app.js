'use strict';
(function(){
    var app = angular.module('app', [
        'controllerModule',
        'ngCookies',
        'ngRoute'
    ]);

    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/Templates/todos.html',
                controller: 'MainController'
            })
            .when('/todo/:id', {
                templateUrl: 'app/Templates/todo.html',
                controller: 'TodoController'
            })
            .when('/login', {
                templateUrl: 'app/Templates/login.html',
                controller: 'LoginController'
            })
            .when('/register', {
                templateUrl: 'app/Templates/register.html',
                controller: 'RegisterController'
            })
            .otherwise('/');
    }]);

    app.run(['$rootScope', '$location', '$cookieStore', '$http', function($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};

        if($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.token;
        }

        if ($rootScope.globals.currentUser && ($location.path() === '/login' || $location.path() === '/register')) {
            $location.path('/');
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.token;
        }
  
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser && $location.path() !== '/register') {
                $location.path('/login');
            }

            if($location.path() === '/register' && $rootScope.globals.currentUser) {
                $location.path('/');   
            }
        });
    }]);

})();