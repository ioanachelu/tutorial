(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['$scope', '$routeParams', '$window', '$http'];

    function TodoController($scope, $routeParams, $window, $http) {
        $scope.todoId = $routeParams.id;

        $scope.todo = {};

        $http.get('/api/v1/todo/' + $scope.todoId).then(function(response) {
            $scope.todo = response.data;
        });

        // $scope.todo = angular.fromJson($window.localStorage['todoList'])[$routeParams.id];
    }
})();