var express = require('express');
var app = express();
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');

var jwt    = require('jsonwebtoken');
var config = require('./node/config');
var User   = require('./node/models/user');

// Configuration
mongoose.connect(config.database); // connect to database
app.set('secretKey', config.secret); // secret variable

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/'));

var restRoutes = express.Router();

restRoutes.post('/authenticate', function(req, res) {
  User.findOne({
    name: req.body.username
  }, function(err, user) {
    if (err) throw err;
    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {
      if (user.password != req.body.password) {
        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
      } else {

        var token = jwt.sign(user, app.get('secretKey'), {
          expiresInMinutes: 1440 // expires in 24 hours
        });
        res.json({
          success: true,
          message: 'Loggin success!',
          token: token
        });
      }   
    }
  });
});

// route middleware to verify a token
restRoutes.use(function(req, res, next) {
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['authorization'];
  if (token) {
    jwt.verify(token, app.get('secretKey'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        req.decoded = decoded;    
        next();
      }
    });

  } else {
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.'
    });
  }
});

app.use('/api/v1', restRoutes);

// register user
app.post('/api/register', function(req, res) {
    if(req.body.username && req.body.password) {
        User.findOne({
            name: req.body.username
        }, function(err, user) {
            if (err) throw err;
            if(!user) {
                var user = new User({ 
                    name: req.body.username, 
                    password: req.body.password
                });
                user.save(function(err) {
                    if (err) throw err;
                        console.log('User saved successfully');
                        res.json({ success: true });
                });
            } else {
                res.json({success: false, message: 'User already exists!'});
            }
        });
    } else {
        res.json({ success: false });
    }
});

app.get('/', function(req, res) {
    res.sendfile('index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");