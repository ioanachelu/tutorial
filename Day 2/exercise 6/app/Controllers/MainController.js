(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule', [])
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$controller', '$window'];

    function MainController($scope, $controller, $window) {

        $scope.todos = angular.fromJson($window.localStorage['todoList']) || [];

        $scope.logout = function() {
            var loginControllerScope = $scope.$new();
            $controller('LoginController',{$scope : loginControllerScope });
            loginControllerScope.logout();
        }

        $scope.addTodo = function() {
            if($scope.todoToAdd) {
                $scope.todos.push({
                    name: $scope.todoToAdd.name,
                    description: $scope.todoToAdd.description,
                    duedate: $scope.todoToAdd.duedate,
                    done: false
                });
                $scope.todoToAdd = undefined;
                $window.localStorage['todoList'] = angular.toJson($scope.todos);
            }
        }

        $scope.removeTodo = function(todo) {
            var index = $scope.todos.indexOf(todo);
            $scope.todos.splice(index, 1);
            $window.localStorage['todoList'] = angular.toJson($scope.todos);
        }

        $scope.toggleChecked = function(todo) {
            todo.done = !todo.done;
            $window.localStorage['todoList'] = angular.toJson($scope.todos);
        }

        $scope.clearCompleted = function() {
            var remaining = [];

            angular.forEach($scope.todos, function(elm, idx) {
                if(!elm.done) {
                    remaining.push(elm);
                }
            });
            $scope.todos = remaining;
            $window.localStorage['todoList'] = angular.toJson(remaining);
        }

    }
})();