(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['$scope', '$routeParams', '$window'];

    function TodoController($scope, $routeParams, $window) {
        $scope.todoId = $routeParams.id;

        $scope.todo = angular.fromJson($window.localStorage['todoList'])[$routeParams.id];
    }
})();