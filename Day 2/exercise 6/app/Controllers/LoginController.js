(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$rootScope', '$cookieStore', '$location', '$http'];

    function LoginController($scope, $rootScope, $cookieStore, $location, $http) {

        $scope.loginError = false;

        $scope.login = function () {
            // Login($scope.username, $scope.password, function(response) {
            //     if(response.success) {
            //         SetCredentials($scope.username, $scope.password);
            //         $location.path('/');
            //     } else {
            //         $scope.error = response.message;
            //     }
            // });

            $http.post('/api/v1/authenticate', {
                username: $scope.username,
                password: $scope.password
            }).then(function(response){
                console.log(response);
                if(response.data.success) {
                    SetCredentials($scope.username, response.data.token);
                    $location.path('/');
                } else {
                    $scope.loginError = true;
                    $scope.message = response.data.message;
                }
            }).catch(function(err) {
                console.log(err);
                $scope.loginError = true;
            });
        };

        $scope.logout = function() {
            ClearCredentials();
            $location.path('/login');
        }

        // function Login(username, password, callback) {
        //     /* Dummy authentication for testing, uses $timeout to simulate api call*/
        //     $timeout(function(){
        //         var response = { success: username === 'test' && password === 'test' };
        //         if(!response.success) {
        //             response.message = 'Username or password is incorrect';
        //         }
        //         callback(response);
        //     }, 1000);
        // };
  
        function SetCredentials(username, token) {
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    token: token
                }
            };
  
            $http.defaults.headers.common['Authorization'] = token;
            $cookieStore.put('globals', $rootScope.globals);
        };

        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
        };

    }
})();