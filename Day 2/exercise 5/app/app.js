'use strict';
(function(){
    var app = angular.module('app', [
        'controllerModule',
        'ngCookies',
        'ngRoute'
    ]);

    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/Templates/todos.html',
                controller: 'MainController'
            })
            .when('/todo/:id', {
                templateUrl: 'app/Templates/todo.html',
                controller: 'TodoController'
            })
            .when('/login', {
                templateUrl: 'app/Templates/login.html',
                controller: 'LoginController'
            })
            .otherwise('/');
    }]);

    app.run(['$rootScope', '$location', '$cookieStore', '$http', function($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser && $location.path() === '/login') {
            $location.path('/');
        }
  
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);

})();