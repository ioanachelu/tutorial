(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$rootScope', '$cookieStore', '$location', '$timeout'];

    function LoginController($scope, $rootScope, $cookieStore, $location, $timeout) {
  
        $scope.login = function () {
            Login($scope.username, $scope.password, function(response) {
                if(response.success) {
                    SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                } else {
                    $scope.error = response.message;
                }
            });
        };

        $scope.logout = function() {
            ClearCredentials();
            $location.path('/login');
        }

        function Login(username, password, callback) {
            /* Dummy authentication for testing, uses $timeout to simulate api call*/
            $timeout(function(){
                var response = { success: username === 'test' && password === 'test' };
                if(!response.success) {
                    response.message = 'Username or password is incorrect';
                }
                callback(response);
            }, 1000);
        };
  
        function SetCredentials(username, password) {
            $rootScope.globals = {
                currentUser: {
                    username: username
                }
            };
  
            $cookieStore.put('globals', $rootScope.globals);
        };

        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
        };

    }
})();