'use strict';
(function(){
    var app = angular.module('app', [
        'controllerModule',
        'ngRoute'
    ]);

    // app.$inject = ['$routeProvider'];

    app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/Templates/todos.html',
                controller: 'MainController'
            })
            .when('/todo/:id', {
                templateUrl: 'app/Templates/todo.html',
                controller: 'TodoController'
            })
            .otherwise('/');
    });

})();