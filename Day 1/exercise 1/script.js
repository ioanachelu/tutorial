document.querySelector('#taskInput').addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if ((key == 13) && (document.querySelector('#taskInput').value.length > 0))
        addToList(this.value.trim());
});

var taskList = [],
    completedTasks = [];

if (JSON.parse(localStorage.getItem('taskList')))
    taskList = JSON.parse(localStorage.getItem('taskList'));
else
    localStorage.setItem("taskList", JSON.stringify(taskList));

updateCompletedListArray();
updateListView();

function updateCompletedListArray() {
    completedTasks = [];

    taskList.forEach(function (task) {
        if (task.done)
            completedTasks.push(taskList.indexOf(task) + '');
    });
}

function addToList(task) {
    if (checkDuplicate(task)) {
        return;
    }

    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

    taskList.push({
        name: task,
        done: false,
        dateAdded: datetime,
        dateModif: datetime
    });

    updateListView();

    localStorage.setItem('taskList', JSON.stringify(taskList));
    document.querySelector('#taskInput').value = '';
}

function updateListView() {
    var ul = document.getElementById('taskList');

    ul.innerHTML = '';

    taskList.forEach(function (task) {
        var listItem = document.createElement('li'),
            taskLabel = document.createElement('label'),
            delBtn = document.createElement('button'),
            checkbox = document.createElement('input'),
            dateModif = document.createElement('label'),
            dateAdded = document.createElement('label');


        listItem.className = 'task';
        listItem.className += ' list-group-item clearfix';
        listItem.id = taskList.indexOf(task);

        taskLabel.className = 'taskLabel';
        taskLabel.textContent = task.name;
        taskLabel.htmlFor = 'c' + taskList.indexOf(task);

        dateAdded.className = 'taskLabel';
        dateAdded.textContent = task.dateAdded;
        dateAdded.htmlFor = 'a' + taskList.indexOf(task);

        dateModif.className = 'taskLabel';
        dateModif.textContent = task.dateModif;
        dateModif.id = 'm' + taskList.indexOf(task);

        delBtn.className = 'deleteTaskBtn pull-right btn btn-danger';
        delBtn.textContent = 'x';
        delBtn.onclick = deleteThisTask;

        checkbox.className = 'taskCheckbox'
        checkbox.id = 'c' + taskList.indexOf(task);
        checkbox.type = 'checkbox';
        checkbox.checked = task.done;
        checkbox.onclick = toggleChecked;
        checkbox.style.marginRight = '20px';

        listItem.appendChild(checkbox);
        listItem.appendChild(taskLabel);
        listItem.appendChild(dateAdded);
        listItem.appendChild(dateModif);
        listItem.appendChild(delBtn);
        ul.appendChild(listItem);
    });
}

function toggleChecked(e) {
    var checkStatus = e.target.checked,
        task = e.target.parentElement,
        taskId = task.id,
        removed = false;
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

    taskList[taskId].done = checkStatus;
    taskList[taskId].dateModif = datetime;

    document.getElementById(('m' + taskId)).innerHTML = datetime;

    if (completedTasks.length === 0) {
        completedTasks.push(taskId);

    } else {
        completedTasks.forEach(function (index) {
            if (taskId === index) {
                completedTasks.splice(completedTasks.indexOf(index), 1);
                removed = true;
            }
        });

        if (!removed) {
            completedTasks.push(taskId);
            completedTasks.sort();
        }
    }

    saveLocalList();
}

function checkDuplicate(task) {
    var matchFound = false;

    taskList.forEach(function (t) {
        if (t.name === task)
            matchFound = true;
    });

    return matchFound;
}

function deleteThisTask(e) {
    taskList.splice(e.target.parentElement.id, 1);

    saveLocalList();
    updateCompletedListArray();
    updateListView();
}

function deleteCompleted() {
    var length = completedTasks.length;

    for (var i = completedTasks.length; i--;) {
        taskList.splice(completedTasks[i], 1);
    }

    saveLocalList();
    updateCompletedListArray();
    updateListView();
}

function deleteAll() {
    if ((taskList.length > 0) && confirm("Are you sure you want to delete all your tasks?")) {
        var ul = document.getElementById('taskList');
        ul.innerHTML = '';
        taskList = completedTasks = [];
        saveLocalList();
    }
}

function saveLocalList() {
    localStorage.setItem("taskList", JSON.stringify(taskList));
}
