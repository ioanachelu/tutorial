(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule', [])
        .controller('MainController', MainController);

    MainController.$inject = ['$scope'];

    function MainController($scope) {

        $scope.todos = [];

        $scope.addTodo = function() {
            if($scope.todoToAdd) {
                $scope.todos.push({
                    name: $scope.todoToAdd.name,
                    done: false
                });
                $scope.todoToAdd = undefined;
            }
        }

        $scope.removeTodo = function(todo) {
            var index = $scope.todos.indexOf(todo)
            $scope.todos.splice(index, 1);
        }

    }

})();