(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$scope', '$http'];

    function RegisterController($scope, $http) {
        $scope.test = 'register';
        $scope.registrationError = false;
        $scope.registrationSuccess = false;

        $scope.register = function() {
            if($scope.username && $scope.password && $scope.password === $scope.check_password) {
                $http.post('/api/register', {
                    username: $scope.username,
                    password: $scope.password
                }).then(function(response) {
                    if(response.data.success) {
                        $scope.registrationSuccess = true;
                        $scope.registrationError = false; 
                        $scope.username = '';
                        $scope.password = '';  
                        $scope.check_password = '';   
                    } else {
                        $scope.registrationSuccess = false;
                        $scope.registrationError = true;
                        $scope.message = response.data.message;
                    }
                }).catch(function(err) {
                    console.log(err);
                    $scope.registrationSuccess = false;
                    $scope.registrationError = true;
                });
            } else {
                $scope.registrationError = true;
                $scope.registrationSuccess = false;
            }
        }
    }
})();