(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule', [])
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$controller', 'TodoService'];

    function MainController($scope, $controller, TodoService) {

        function getTodos() {
            $scope.todos = {};
            TodoService.getTodos().then(function(response) {
                $scope.todos = response.data;
            });
        }

        getTodos();

        $scope.addTodo = function() {
            if($scope.todoToAdd) {
                TodoService.addTodo({
                    name: $scope.todoToAdd.name,
                    description: $scope.todoToAdd.description,
                    duedate: $scope.todoToAdd.duedate
                }).then(function(response) {
                    $scope.todoToAdd = undefined;
                    getTodos();
                    $scope.todoForm.$setUntouched();
                    $scope.todoForm.$setPristine();
                });
            }
        }

        $scope.removeTodo = function(todo) {
            TodoService.removeTodo(todo._id).then(function(response) {
                getTodos();
            });
        }

        $scope.toggleChecked = function(todo) {
            todo.done = !todo.done;
        }

    }
})();