(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$location', 'AuthService'];

    function LoginController($scope, $location, AuthService) {

        $scope.loginError = false;

        $scope.login = function () {
            AuthService.login($scope.username, $scope.password).then(function(response){
                if(response.data.success) {
                    AuthService.setCredentials($scope.username, response.data.token);
                    $location.path('/');
                } else {
                    $scope.loginError = true;
                    $scope.message = response.data.message;
                }
            });
        };

        $scope.logout = function() {
            AuthService.clearCredentials();
            $location.path('/login');
        }
    }
})();