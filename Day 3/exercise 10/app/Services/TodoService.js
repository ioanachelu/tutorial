(function(){
    'use strict';

    var serviceModule = angular.module('serviceModule')
        .factory('TodoService', TodoService);

    TodoService.$inject = ['$http'];

    function TodoService($http) {
        var service = {};
        
        service.getTodo = function(id) {
            return $http.get('/api/v1/todo/' + id);
        }

        service.getTodos = function() {
            return $http.get('api/v1/todos');
        }

        service.addTodo = function(params) {
            return $http.post('api/v1/todo', params);
        }

        service.removeTodo = function(id) {
            return $http.delete('/api/v1/deletetodo/' + id);
        }

        return service;
    }

})();