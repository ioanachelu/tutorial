(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['$scope', '$routeParams', 'TodoService'];

    function TodoController($scope, $routeParams, TodoService) {
        $scope.todoId = $routeParams.id;

        TodoService.getTodo($scope.todoId).then(function(response) {
            $scope.todo = response.data;
        });

    }
})();