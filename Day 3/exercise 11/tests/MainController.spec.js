describe('MainController test', function() {
    var TodoService, httpBackend, scope, ctrl;

    beforeEach(function(){
        
        module('app');    
        
        module(function($provide) {
            $provide.value('TodoService', {
                getTodos: function() {
                    return { 
                        then: function(callback) {return callback({"data":[{"_id":"5591d936019e71402bef4442","name":"some todo","user_id":"5591d909019e71402bef4441","__v":0,"done":false,"duedate":null,"description":"Some todo description"}],"status":200,"config":{"method":"GET","transformRequest":[null],"transformResponse":[null],"url":"api/v1/todos","headers":{"Accept":"application/json, text/plain, */*","Authorization":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NTkxZDkwOTAxOWU3MTQwMmJlZjQ0NDEiLCJuYW1lIjoiSm9obiIsInBhc3N3b3JkIjoidGVzdCIsIl9fdiI6MH0.T6rjS_Lj9BdlsUekb0q0hFeNwaMdEeCPCXHs4NoTj7o"}},"statusText":"OK"})}
                    };
                }
            });
        });

        inject(function($rootScope, $controller, _TodoService_){
            scope = $rootScope.$new();
            TodoService = _TodoService_;

            spyOn(TodoService, 'getTodos').and.callThrough();

            ctrl = $controller('MainController', {
                $scope: scope, 
                TodoService: TodoService
            });
        });
    });

    it("should get todos", function(){
        TodoService.getTodos()
        expect(TodoService.getTodos).toHaveBeenCalled();
        expect(scope.todos.length).toBe(1);
        expect(scope.todos[0].name).toBe('some todo')
    });
});