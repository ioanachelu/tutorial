describe('AuthService test', function() {
    var AuthService, httpBackend;

    beforeEach(function(){
		module('app');

        inject(function($httpBackend, _AuthService_) {
            AuthService = _AuthService_;
            httpBackend = $httpBackend;
        });
	});

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("modules should be defined", function(){
    	var returnData = { success: true };
        
        httpBackend.expectPOST('/api/v1/authenticate').respond(returnData);

        var returnedPromise = AuthService.login();

        var result;
        returnedPromise.then(function(response) {
            result = response.data;
        });
        
        httpBackend.flush();
        expect(result).toEqual(returnData);
    });
});