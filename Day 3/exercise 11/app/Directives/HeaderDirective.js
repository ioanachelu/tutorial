angular.module("directiveModule", [])

.directive("customHeader", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: 'app/Templates/header.html',
        controller: function($scope, $cookieStore, $controller) {
            $scope.username = $cookieStore.get('globals').currentUser.username;

            console.log($scope.username)

            $scope.logout = function() {
                var loginControllerScope = $scope.$new();
                $controller('LoginController',{$scope : loginControllerScope });
                loginControllerScope.logout();
            }
        }
    };
});