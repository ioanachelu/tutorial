(function(){
    'use strict';

    var controllerModule = angular.module('controllerModule', [])
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$controller', '$window', '$http'];

    function MainController($scope, $controller, $window, $http) {

        $scope.todos = {};

        function getTodos() {
            $http.get('api/v1/todos').then(function(response) {
                $scope.todos = response.data;
            });
        }

        getTodos();

        $scope.logout = function() {
            var loginControllerScope = $scope.$new();
            $controller('LoginController',{$scope : loginControllerScope });
            loginControllerScope.logout();
        }

        $scope.addTodo = function() {
            if($scope.todoToAdd) {
                $http.post('api/v1/todo', {
                    name: $scope.todoToAdd.name,
                    description: $scope.todoToAdd.description,
                    duedate: $scope.todoToAdd.duedate
                }).then(function(response) {
                    $scope.todoToAdd = undefined;
                    getTodos();
                    $scope.todoForm.$setUntouched();
                    $scope.todoForm.$setPristine();
                });
            }
        }

        $scope.removeTodo = function(todo) {
            $http.delete('/api/v1/deletetodo/' + todo._id).then(function(response) {
                getTodos();
            });
        }

        $scope.toggleChecked = function(todo) {
            todo.done = !todo.done;
            $window.localStorage['todoList'] = angular.toJson($scope.todos);
        }

        $scope.clearCompleted = function() {
            var remaining = [];

            angular.forEach($scope.todos, function(elm, idx) {
                if(!elm.done) {
                    remaining.push(elm);
                }
            });
            $scope.todos = remaining;
            $window.localStorage['todoList'] = angular.toJson(remaining);
        }

    }
})();