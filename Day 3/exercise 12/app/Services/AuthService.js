(function(){
    'use strict';

    var serviceModule = angular.module('serviceModule', [])
        .service('AuthService', AuthService);

    AuthService.$inject = ['$http', '$cookieStore', '$rootScope'];

    function AuthService($http, $cookieStore, $rootScope) {
        this.login = function(username, password) {
            return $http.post('/api/v1/authenticate', {
                username: username,
                password: password
            });
        }

        this.setCredentials = function(username, token) {
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    token: token
                }
            };

            $http.defaults.headers.common['Authorization'] = token;
            $cookieStore.put('globals', $rootScope.globals);
        }

        this.clearCredentials = function() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
        }

    }

})();