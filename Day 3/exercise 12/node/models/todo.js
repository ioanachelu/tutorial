var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Todo', new Schema({ 
    name: String, 
    description: { type: String, default: ''},
    duedate: { type: String, default: null},
    done:{ type: Boolean, default: false},
    user_id: String
}));