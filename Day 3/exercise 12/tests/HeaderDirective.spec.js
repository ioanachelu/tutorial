describe('HeaderDirective test', function() {
    var $scope, cookieStore, element;

    beforeEach(function() {
        module('app');
    });

    beforeEach(inject(function($rootScope, $compile, $cookieStore, $httpBackend) {

        $httpBackend.whenGET('app/Templates/header.html').respond(200, "it works");

        $scope = $rootScope.$new();
        cookieStore = $cookieStore;

        var globals = {
            currentUser: {
                username: 'Mark',
                token: '123412341234'
            }
        };

        cookieStore.put('globals', globals);

        element = angular.element("<div></div>");
        template = $compile(element)($scope);
        $scope.$digest();
        controller = element.controller;
        
    }));

    it('should get the username', function() {
        expect(element).toBeDefined()
    });
});