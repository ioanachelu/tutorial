describe('AngularJS App', function() {

    var todos = element.all(by.repeater('todo in todos'));
    var submit = element(by.buttonText('Add'));
    var nameField = element(by.model('todoToAdd.name'));
    var description = element(by.model('todoToAdd.description'));

    beforeEach(function(){
        browser.get('http://localhost:8080/');
        var username = element(by.model('username'));
        var password = element(by.model('password'));
        var signin = element(by.buttonText('Sign in'));

        username.sendKeys('John');
        password.sendKeys('test');

        signin.click();
    });

    afterEach(function(){
        var logout = element(by.buttonText('Logout'));
        logout.click();
    });

    it('should have a title', function() {
        expect(browser.getTitle()).toEqual('AngularJS Basic');
    });

    it('should add a todo', function() {
        var initialCount = todos.count();

        nameField.sendKeys('wash car');
        description.sendKeys('some todo description');

        submit.click().then(function() {
            expect(todos.count()).toBeGreaterThan(initialCount);
        });
    });

});